import { createApp } from 'vue';
import store from './store/index';
import App from './App.vue';
import ApiService from './commons/api.service';

const app = createApp(App);
ApiService.init(app);
app.use(store);
app.mount('#app');


import axios from 'axios';
import VueAxios from 'vue-axios';
import CONFIG from './config.js';

const ApiService = {
  init(app) {
    app.use(VueAxios, axios);
    app.axios.defaults.baseURL = CONFIG.API_BASE_URL;
    app.axios.defaults.params = {};
    app.axios.defaults.params['apiKey'] = CONFIG.API_KEY;
  },

  query(resource, params) {
    return axios.get(resource, { params }).catch((error) => {
      throw new Error(`API SERVICE: ${error}`);
    });
  },
};

export default ApiService;


import ApiService from '../api.service';
import CONFIG from '../config';

export const newsListService = {
  get(params) {
    return ApiService.query(CONFIG.API_URLS.topHeadlines, params);
  },
};

export default newsListService;

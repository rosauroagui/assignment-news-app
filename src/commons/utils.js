function getFormattedDate(date) {
  return new Date(date).toISOString().split('T')[0].replaceAll('-', '/');
}
export { getFormattedDate };
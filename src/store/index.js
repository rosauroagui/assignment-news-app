import { createStore } from 'vuex';
import newsListModule from './modules/newsList.module';

const store = createStore({
  modules: {
      newsListModule,
  },
  isLoading : false,
});

export default store;

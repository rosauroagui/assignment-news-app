export const FETCH_NEWS_LIST = 'fetchNewsLists';
export const FETCH_NEWS_LIST_BY_FILTER = 'fetchNewsListsByFilter';
export const SET_LOADER = 'setLoader';
export const SET_NEWS_LIST = 'setNewsLists';
export const SET_NEWS_MODAL = 'setNewsModal';
export const SET_NEWS_MODAL_DATA = 'setNewsModalData';

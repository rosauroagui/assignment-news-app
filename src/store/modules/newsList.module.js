import newsListService from '@/commons/modules/newsList.service';
import {
  FETCH_NEWS_LIST,
  SET_NEWS_LIST,
  SET_LOADER,
  SET_NEWS_MODAL_DATA,
  SET_NEWS_MODAL,
} from '../action.type';

const state = {
  newsLists: [],
  isLoading: false,
  newsModal: null,
};

const actions = {
  async [FETCH_NEWS_LIST](context, params) {
    await context.commit(SET_LOADER, true);
    const resp = await newsListService.get(params);
    await context.commit(SET_NEWS_LIST, resp.data.articles);
    await context.commit(SET_LOADER, false);
  },
  async [SET_NEWS_MODAL](context, data) {
    await context.commit(SET_NEWS_MODAL_DATA, data);
  },
};

export const mutations = {
  [SET_NEWS_LIST](state, newsList) {
    state.newsLists = newsList;
  },
  async [SET_LOADER](state, loader) {
    state.isLoading = loader;
  },
  async [SET_NEWS_MODAL_DATA](state, data) {
    state.newsModal = data;
  },
};

const getters = {
  getNewsLists(state) {
    return state.newsLists;
  },
  getLoader(state) {
    return state.isLoading;
  },
  getNewsModalData(state) {
    return state.newsModal;
  },
};

export default {
  state,
  mutations,
  getters,
  actions,
};
